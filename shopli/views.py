from flask import Blueprint, jsonify


api = Blueprint('api', __name__)  # pylint: disable=invalid-name


@api.route('/', methods=['GET'])
def index():
    """Entry point."""
    return jsonify()
