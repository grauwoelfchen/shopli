import multiprocessing
import os

import gunicorn.app.base

from shopli import create_app


class WSGIApplication(gunicorn.app.base.BaseApplication):
    # pylint: disable=abstract-method
    """
    See http://docs.gunicorn.org/en/latest/custom.html
    """

    def __init__(self, application, options=None):
        self.options = options or {}
        self.application = application
        super(WSGIApplication, self).__init__()

    def load_config(self):
        config = {k: v for k, v in self.options.items()
                  if k in self.cfg.settings}

        for k, v in config.items():
            self.cfg.set(k.lower(), v)

    def load(self):
        return self.application


if __name__ == '__main__':
    # pylint: disable=invalid-name
    server_options = {
        'bind': '{}:{}'.format('127.0.0.1', '5000'),
        'workers': multiprocessing.cpu_count() * 2 + 1,
    }
    app = create_app(os.getenv('ENV', 'development'))
    server = WSGIApplication(app, server_options)
    server.run()
