from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from shopli.config import config
from shopli.views import api


db = SQLAlchemy()  # pylint: disable=invalid-name


def create_app(config_name):
    app = _configure_app(Flask(__name__), config_name)
    db.init_app(app)
    app.register_blueprint(api)

    return app


def _configure_app(app, config_name):
    c = config[config_name]
    app.config.from_object(c)

    c.init_app(app)
    return app
