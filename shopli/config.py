from flask_dotenv import DotEnv


class Config:
    @classmethod
    def init_app(cls, app):
        env = DotEnv()
        env.init_app(app, env_file=None, verbose_mode=False)

        prefix = cls.__name__.replace('Config', '').upper()

        # NOTE:
        # Loads env variables from `os.environ` on **CI server**.
        # In development on local and in production, use `.env` file.
        if prefix == 'TEST':
            import os
            secret_variables = (
                'TEST_DATABASE_URL',
            )
            for name in secret_variables:
                if name in os.environ:
                    app.config.update(**{name: os.environ[name]})

        env.alias({
            prefix + '_DATABASE_URL': 'SQLALCHEMY_DATABASE_URI'
        })


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = None


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = None


class ProductionConfig(Config):
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = None


# pylint: disable=invalid-name
config = {
    'development': DevelopmentConfig,
    'test': TestConfig,
    'production': ProductionConfig,
}
