# Shopli

[![pipeline status](https://gitlab.com/grauwoelfchen/shopli/badges/master/pipeline.svg)](https://gitlab.com/grauwoelfchen/shopli/commits/master)

A shopping list backend API.


## Requirements

* Python `^3.6`
* SQLite `^3`


## Setup

```zsh
% cd /path/to/shopli

% python -m venv venv
% source venv/bin/activate

(venv) % pip install -U pip setuptools

# default: ENV=development
(venv) % make setup
```

## Development

### Serve

```zsh
(venv) % cp .env.sample .env

# use gunicorn
(venv) % make serve
```

### Lint

See `.pylintrc`.

```zsh
(venv) % make lint
```


## Testing

```zsh
# set TEST_* variables
(venv) % cp .env.sample .env

(venv) % ENV=test make setup
(venv) % make test
```


## License

`AGPL-3.0`

```txt
Shopli
Copyright 2018 Yasuhiro Asaka
```

```txt
This is free software: You can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
```

See [LICENSE](LICENSE).
