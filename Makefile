ifeq (, $(ENV))
	ENV := development
	env := development
else ifeq (test, $(ENV))
	env := testing
else
	env := $(ENV)
endif

setup:
	pip install -e '.[${env}]' -c constraints.txt
.PHONY: setup

# development
serve:
	ENV=development python shopli/wsgi.py
.PHONY: serve

check:
	pycodestyle test shopli
.PHONY: check

lint:
	pylint test shopli
.PHONY: lint

vet: | check lint
.PHONY: vet

clean:
	find . ! -readable -prune -o \
	  ! -path "./.git/*" ! -path "./venv*" ! -path "./log/*" -print | \
	  grep -E "(__pycache__|\.egg-info|\.pyc|\.pyo)" | \
	  xargs rm -rf
.PHONY: clean

test:
	ENV=test py.test -c 'pytest.ini' -s -q test
.PHONY: test
