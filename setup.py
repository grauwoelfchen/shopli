# pylint: disable=invalid-name
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'CHANGELOG')) as f:
    CHANGELOG = f.read()

requires = [
    'Flask',
    'Flask-DotEnv',
    'Flask-SQLAlchemy',
]

development_requires = [
    'gunicorn',

    'pycodestyle',
    'pylint',
]

testing_requires = [
    'pytest',
]

production_requires = [
]

setup(
    name='shopli',
    version='0.0.1',
    description='A shopping list backend API',
    long_description='CHANGELOG' + '\n\n' + CHANGELOG,
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Flask',
    ],
    author='Yasuhiro Asaka',
    author_email='yasuhiro.asaka@grauwoelfchen.net',
    url='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'development': development_requires,
        'testing': testing_requires,
        'production': production_requires,
    },
    install_requires=requires,
    entry_points='',
)
