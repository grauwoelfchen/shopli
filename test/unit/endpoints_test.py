import pytest


@pytest.fixture(autouse=True)
def setup(request):
    # NOTE: setup test data etc.

    def teardown():
        pass

    request.addfinalizer(teardown)


def test_index_response(test_client):
    res = test_client.get('/')
    assert b'{}\n' == res.data
