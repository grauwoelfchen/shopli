import pytest

from shopli import create_app


@pytest.fixture
def test_client():
    """Returns testing app."""
    app = create_app('test')
    return app.test_client()
